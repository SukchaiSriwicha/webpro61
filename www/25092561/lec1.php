<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<!-- <script>
let user = {name: "John",age: 30};

let key = prompt("What do you want to know about the user?", "name");

// access by variable
alert( user[key] ); // John (if enter "name")
	</script> -->

	<!-- <script>	
		function makeUser(name, age) {
  return {name: name,age: age};
    // ...other properties
}

let user = makeUser("John", 30);
alert(user.name); // John
	</script> -->

<!-- 	<script>
		let guestList = `Guests:
 * John
 * Pete
 * Mary
`;

alert(guestList); // a list of guests, multiple lines
	</script> -->


<!-- <script>
	
	let str = 'Hi';

str[0] = 'h';
alert( str[0] );
</script> -->

<script>
	let str = 'Widget with id';

alert( str.indexOf('Widget') ); // 0, because 'Widget' is found at the beginning
alert( str.indexOf('widget') ); // -1, not found, the search is case-sensitive

alert( str.indexOf("id") ); // 1, "id" is found at the position 1 (..idget with id)
</script>

</body>
</html>